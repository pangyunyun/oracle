# 实验2：用户及权限管理

- 学号：202010414313，姓名：庞芸芸，班级：软工3班
  
## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

```sql

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:30:20 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 3月  07 2023 16:25:59 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> CREATE ROLE con_res_role;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;

授权成功。

SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。

SQL> ALTER USER sale default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> GRANT con_res_role TO sale;

授权成功。

```
![a1.png](./a1.png)

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。

```sql
$ sqlplus sale/123@pdborcl
SQL> show user;
USER 为 "SALE"
SELECT * FROM session_privs;
SELECT * FROM session_roles;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。

SQL> 
ROLE
--------------------------------------------------------------------------------
CON_RES_ROLE
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;

表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang

```
![a2.png](./a2.png)

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
SELECT * FROM sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在


SQL> SELECT * FROM sale.customers_view;

NAME
--------------------------------------------------
zhang
wang

```

![a3.png](./a3.png)

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl
SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      440    94.8125   345.1875      78.45
UNDOTBS1			      100	  35	     65 	65
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260     7.8125   252.1875 	97

```

![a4.png](./a4.png)

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。
  
## 实验结束，删除用户和角色
```sql
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```


## 实验结论

- 用户创建后不能直接登录数据库及对数据库进行操作，需要赋予用户一定的权限才可以进行相应的操作。
- 角色是一组相关权限的命名集合，使用角色最主要的目的是简化权限管理。将一组权限打包到角色中，赋权时可直接将角色赋予用户，代表将角色下得所有权限都赋予了用户，简化了赋权操作
- 权限分为两类
    + 系统权限: 允许用户执行特定的数据库动作，如创建表、创建索引、连接实例等
    + 对象权限: 允许用户操纵一些特定的对象，如读取视图，可更新某些列、执行存储过程等
- 通过这次实验，我了解到了Oracle中的一些有关用户操作的功能，例如用户管理、角色管理、权根维护与分配的能力，在实验过程中我还掌握了用户之间共享对象的操作技能。在实验完成后，能够独自完成一些简单的oracle数据库用户操作。
