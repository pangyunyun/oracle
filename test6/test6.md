# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

**【小提示：实验过程截图都在Word文档展示】**

## 一、表设计
1.商品信息表(good)
|字段  |说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|sid	|商品编号	|number 	|11	|主键|
|sname	|商品名称	|Varchar2	|20	|否|
|stid	|类别ID  	|number	   |11	|外键|
|sinfo	|商品描述	|Varchar2	|100|否|
|sprice	|商品价格	|number 	|8	|否
|spicture|商品图片  |Varchar2	|50	|否|

（1）建表的SQL命令：
```sql
CREATE TABLE `good` (
  `sid` number(11) NOT NULL,
  `sname` varchar2(20) NOT NULL,
  `stid` number(11) NOT NULL,
  `sinfo` varchar2(50) NOT NULL,
  `sprice` number(8,2) NOT NULL,
  `spicture` varchar2(20) NOT NULL,
  PRIMARY KEY (`sid`)
);
```
（2）插入语句的SQL命令:
```sql
INSERT INTO good VALUES(1,'条纹衬衫',9,'条纹短袖衬衫男夏季古巴领高级感英伦男装',78.88,'./images/pic.jpg');
INSERT INTO good VALUES(2,'长筒袜',8,'潮袜韩版长筒棉袜女春夏季嘻哈运动',29.80,'./images/pic.jpg');
INSERT INTO good VALUES(3,'一轮玫瑰香水',28,'一轮玫瑰贵妇肖像雨后当归苦橙法国情',199.99,'./images/pic.jpg');
INSERT INTO good VALUES(4,'小狐狸手绳',15,'檀木小狐狸手链绳森系原创定制饰品',118.00,'./images/pic.jpg');
INSERT INTO good VALUES(5,'特步运动鞋',24,'特步运动鞋夏季新款女鞋轻便休闲透气跑鞋女',319.00,'./images/pic.jpg');
INSERT INTO good VALUES(6,'华为手环8',20,'智能超薄血氧检测全面屏快充长续航',399.00,'./images/pic.jpg');
INSERT INTO good VALUES(7,'情侣项链',14,'情侣款高级感520礼物送女友',209.00,'./images/pic.jpg');
INSERT INTO good VALUES(8,'山茶花沐浴露',17,'山茶花沐浴露持久留香',68.00,'./images/pic.jpg');
INSERT INTO good VALUES(9,'伊利纯牛奶',26,'伊利无菌砖牛奶优质乳蛋白',57.6,'./images/pic.jpg');
INSERT INTO good VALUES(10,'蓝月亮洗衣液',16,'自然清香家用持久护理',42.00,'./images/pic.jpg');
```

2.类别表(type)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|tid	|类别ID	|number	11	|主键|
|tname	|类别名称	|Varchar2	|20	|否|
|ftid	|父类别ID	|number	|11	|外键|

（1）建表的SQL命令：
```sql
CREATE TABLE `type` (
  `tid` number(11) NOT NULL,
  `tname` varchar2(20) NOT NULL,
  `ftid` number(11) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO type VALUES(1,'服装',0);
INSERT INTO type VALUES(2,'饰品',0);
INSERT INTO type VALUES(3,'洗护',0);
INSERT INTO type VALUES(4,'数码',0);
INSERT INTO type VALUES(5,'运动',0);
INSERT INTO type VALUES(6,'食品',0);
INSERT INTO type VALUES(7,'美妆',0);
INSERT INTO type VALUES(8,'袜子',1);
INSERT INTO type VALUES(9,'衬衫',1);
INSERT INTO type VALUES(10,'裤子',1);
INSERT INTO type VALUES(11,'裙子',1);
INSERT INTO type VALUES(12,'耳饰',2);
INSERT INTO type VALUES(13,'发饰',2);
INSERT INTO type VALUES(14,'项链',2);
INSERT INTO type VALUES(15,'手链',2);
INSERT INTO type VALUES(16,'洗衣护理',3);
INSERT INTO type VALUES(17,'沐浴露',3);
INSERT INTO type VALUES(18,'洗发水',3);
INSERT INTO type VALUES(19,'清洁工具',3);
INSERT INTO type VALUES(20,'手表',4);
INSERT INTO type VALUES(21,'笔记本',4);
INSERT INTO type VALUES(22,'平板',5);
INSERT INTO type VALUES(23,'音响',5);
INSERT INTO type VALUES(24,'跑步鞋',5);
INSERT INTO type VALUES(25,'运动裤',5);
INSERT INTO type VALUES(26,'纯牛奶',6);
INSERT INTO type VALUES(27,'饼干',6);
INSERT INTO type VALUES(28,'香水',7);
INSERT INTO type VALUES(29,'口红',7);
```


3.用户信息表(user_table)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|userid	|用户ID |number|	11|	主键|
|uname	|用户名|	Varchar2|	20|	否|
|upassword|	密码|	Varchar2|	20|	否|
|is_vip	|是否为会员	|number	|11|	否|
|upicture	|头像	|Varchar2|	20|	否|

（1）建表的SQL命令：
```sql
CREATE TABLE `user_table` (
  `userid` number(11) NOT NULL,
  `uname` varchar2(20) NOT NULL,
  `upassword` varchar2(20) NOT NULL,
  `is_vip` number(11) DEFAULT '0',
  `upicture` varchar(20) NOT NULL,
  PRIMARY KEY (`uid`)
) ;

（2）插入语句的SQL命令:
INSERT INTO user_table VALUES(1,'user1','123456',1,'./images/my.jpg');
INSERT INTO user_table VALUES(2,'user2','123456',0,'./images/my.jpg');
INSERT INTO user_table VALUES(3,'user3','123456',0,'./images/my.jpg');
```


4.购物车表(car)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|id	|自增ID	|number	|11	|主键|
|userid	|用户ID|number|	11	|外键|
|sid	|商品编号|	number|	11|	外键|
|saleprice|	单价|	number|	8	|否|
|num	|数量	|number|	11|	否|

（1）建表的SQL命令：
```sql
CREATE TABLE `car` (
  `id` number(11) NOT NULL,
  `userid` number(11) NOT NULL,
  `sid` number(11) NOT NULL,
  `saleprice` number(8,2) NOT NULL,
  `num` number(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO car VALUES(1,1,1,79.88,1);
INSERT INTO car VALUES(2,1,2,29.80,3);
INSERT INTO car VALUES(3,1,7,209.00,1);
INSERT INTO car VALUES(4,1,6,399,2);
INSERT INTO car VALUES(5,2,10,42.00,1);
```


5.订单表(orders)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|oid	|订单号	|number|	11	|主键|
|userid	|用户ID	|number	|11	|外键|
|sid	|商品编号|	number|	11|	外键|
|saleprice	|单价|	number|	8	|否|
|tranprice	|运费|	number|	11|	否|
|num	|数量	|number|	11|	否|
|allprice	|总付款	|number|	11|	否|
|time	|订单创建时间|	timestamp|	7	|否|
|ostatus	|订单状态|	Varchar2|	20	|否|
|cid	|地址	|number|	11	|外键|

（1）建表的SQL命令：
```sql
CREATE TABLE `orders` (
  `oid` number(11) NOT NULL,
  `userid` number(11) NOT NULL,
  `sid` number(11) NOT NULL,
  `saleprice` number(8,2) NOT NULL,
  `tranprice` number(11) NOT NULL,
  `num` number(11) NOT NULL,
  `allprice` number(10,2) NOT NULL,
  `time` timestamp NOT NULL,
  `ostatus` varchar2(20) NOT NULL,
  `cid` number(11) NOT NULL,
  PRIMARY KEY (`oid`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO orders VALUES(1,1,3,199.99,0,1,199.99,'19-5月 -23 09.36.13.103074000 上午','待发货',1);
INSERT INTO orders VALUES(2,1,4,118.00,5,1,123.00,'19-5月 -23 09.36.13.103074000 上午','待收货',2);
```


6.评论表(comments)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|id	|自增ID	|number	|11	|主键|
|userid	|用户编号	|number|	11|	外键|
|sid	|商品编号	|number	|11	|外键|
|content|	评论内容|Varchar2|200	|否|
|grade	|用户评分	|number|2	|否|
|time	|评论时间	|datetime	|7	|否|

（1）建表的SQL命令：
```sql
CREATE TABLE `comments` (
  `id` number(11) NOT NULL,
  `userid` number(11) NOT NULL,
  `sid` number(11) NOT NULL,
  `content` varchar2(200) NOT NULL,
  `grade` number(11) DEFAULT '5',
  `time` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO comments VALUES(1,2,6,'手环很好用，支持华为',5,'18-5月 -23 09.36.13.103074000 上午');
INSERT INTO comments VALUES(2,2,5,'鞋子挺好看，就是不太透气',4,'21-5月 -23 09.36.13.103074000 上午');
INSERT INTO comments VALUES(3,3,2,'袜子穿几天就起球了，差评！！',1,'21-5月 -23 09.36.13.103074000 上午');
```


7.供应商表(supplier)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|spid	|供应商编号|	number|	11|	主键|
|spname	|供应商名称|	Varchar2|	50|	否|
|address	|供应商地|	Varchar2|	100|否|
|phone	|供应商联系电话|	Varchar2|	11|	否|

（1）建表的SQL命令：
```sql
CREATE TABLE `supplier` (
  `spid` number(11) NOT NULL,
  `spname` varchar2(20) NOT NULL,
  `address` varchar2(50) NOT NULL,
  `phone` varchar2(11) NOT NULL,
  PRIMARY KEY (`spid`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO supplier VALUES(1,'深圳艾睿科技有限公司','深圳XX区XX路XX号','18511111111');
INSERT INTO supplier VALUES(2,'福建泉州服装有限公司','泉州XX区XX路XX号','17811111111');
INSERT INTO supplier VALUES(3,'江苏珠宝有限公司','江苏XX区XX路XX号','17311111111');
INSERT INTO supplier VALUES(4,'广州食品有限公司','广州XX区XX路XX号','19111111111');
INSERT INTO supplier VALUES(5,'广州家居有限公司','广州XX区XX路XX号','18311111111');
```


8.地址表(address)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|cid	|自增ID	|number	|11	|主键|
|userid	|关联用户ID	|number	|11	|外键|
|cname	|收货人姓名|Varchar2|20|否|
|caddr	|收货地址|Varchar2|100|否|
|cphone	|手机号	|Varchar2|11|否|
|is_common|是否默认常用|number|11|否|

（1）建表的SQL命令：
```sql
CREATE TABLE `address` (
  `cid` number(11) NOT NULL,
  `userid` number(11) NOT NULL,
  `cname` varchar2(20) NOT NULL,
  `caddr` varchar2(100) NOT NULL,
  `cphone` varchar2(11) NOT NULL,
  `is_common` number(11) DEFAULT '0',
  PRIMARY KEY (`cid`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO address VALUES(1,1,'咸鱼','四川省成都市XX区XX路XX号','18711111111',1);
INSERT INTO address VALUES(2,1,'监护人','四川省泸州市XX区XX路XX号','18711111111',0);
INSERT INTO address VALUES(3,1,'小乖','江苏省南京市XX区XX路XX号','18511111111',0);
INSERT INTO address VALUES(4,2,'乐乐','四川省成都市XX区XX路XX号','18111111111',1);
INSERT INTO address VALUES(5,3,'笑笑','四川省成都市XX区XX路XX号','18911111111',1);
```


9.物流表(transport)
|字段|	说明|	类型|	长度|	主外键|
|:-----|:-----|:-----|:-----|:-----|
|tid	|物流编号	|number|11|	主键|
|oid	|订单编号	|number|11|	外键|
|saddr	|发货地	|Varchar2|100|	否|
|cpayment|	支付方式|Varchar2|20|	否|
|cdistribution|	配送方式|Varchar2|	20|	否|
|cstatus	|物流状态	|Varchar2|	20|	否|

（1）建表的SQL命令：
```sql
CREATE TABLE `transport` (
  `tid` number(11) NOT NULL,
  `oid` number(11) NOT NULL,
  `saddr` varchar2(50) NOT NULL,
  `cpayment` varchar2(20) NOT NULL,
  `cdistribution` varchar2(20) NOT NULL,
  `cstatus` varchar2(20) NOT NULL,
  PRIMARY KEY (`tid`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO transport VALUES(1,2,'福建泉州XX区XX路XX号','支付宝','圆通速递','配送中');
```


10.店铺表(store)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|id	|店铺编号|	number|	11|	主键|
|storename|	店铺名|	Varchar2|20	|否|
|fan|	粉丝数|	number|	11|	否|
|grade|	综合评分|	number|	2|否|

（1）建表的SQL命令：
```sql
CREATE TABLE `store` (
  `id` number(11) NOT NULL,
  `storename` varchar2(20) NOT NULL,
  `fan` number(11) NOT NULL,
  `grade` number(2,1) NOT NULL,
  PRIMARY KEY (`id`)
);
```
（2）插入语句的SQL命令:
```sql
INSERT INTO store VALUES(1,'手作杂货铺',1820900,5.0);
INSERT INTO store VALUES(2,'植护旗舰店',67769,4.8);
INSERT INTO store VALUES(3,'潮牌原创设计',490,4.1);
INSERT INTO store VALUES(4,'天猫旗舰店',1720540,5.0);
INSERT INTO store VALUES(5,'珠宝旗舰店',43450,5.0);
```


11.库存表(stock)
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|id	|自增ID|	number|	11	|主键|
|sid|	商品编号|	number|	11|	外键|
|spid|	供应商编号|	number|	11	|外键|
|time|	入库时间|timestamp|	7	|否|
|num|	数量|number	|11	|否|

（1）建表的SQL命令：
```sql
CREATE TABLE `stock` (
  `id` number(11) NOT NULL,
  `sid` number(11) DEFAULT NULL,
  `spid` number(11) NOT NULL,
  `time` timestamp NOT NULL,
  `num` number(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;
```
（2）插入语句的SQL命令:
```sql
INSERT INTO stock VALUES(1,1,2,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(2,2,2,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(3,3,5,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(4,4,3,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(5,5,2,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(6,6,1,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(7,7,3,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(8,8,5,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(9,9,4,'19-5月 -23 09.36.13.103074000 上午',1000);
INSERT INTO stock VALUES(10,10,5,'19-5月 -23 09.36.13.103074000 上午',1000);
```


12.店铺售卖商品表（stgood）
|字段	|说明	|类型	|长度	|主外键|
|:-----|:-----|:-----|:-----|:-----|
|id	|货物编号|	number|	11|	主键|
|storeid|	店铺编号|number|11|	外键|
|sid	|商品编号|	number|	11|	外键|

（1）建表的SQL命令：
```sql
CREATE TABLE `stock` (
  `id` number(11) NOT NULL,
  `storeid` number(11) DEFAULT NULL,
  `sid` number(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;
```
(2)插入语句的SQL命令:
```sql
INSERT INTO stgood VALUES(1,1,4);
INSERT INTO stgood VALUES(1,2,10);
INSERT INTO stgood VALUES(1,2,3);
INSERT INTO stgood VALUES(1,3,1);
INSERT INTO stgood VALUES(1,3,7);
INSERT INTO stgood VALUES(1,3,4);
INSERT INTO stgood VALUES(1,3,5);
INSERT INTO stgood VALUES(1,4,2);
INSERT INTO stgood VALUES(1,4,7);
INSERT INTO stgood VALUES(1,4,8);
INSERT INTO stgood VALUES(1,4,9);
INSERT INTO stgood VALUES(1,4,10);
INSERT INTO stgood VALUES(1,4,6);
INSERT INTO stgood VALUES(1,5,7);
```

## 二、用户及权限管理
1.角色：管理员\
权限：可以对所有表进行增改查，可以创建过程、触发器、视图\
步骤：\
（1）以system登录到pdborcl，创建角色manage_role和用户manage，并且授权和空间分配：
```
$ sqlplus system/123@pdborcl
CREATE ROLE manage_role;
GRANT connect,resource,create session,CREATE VIEW TO manage_role;
CREATE USER manage IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER manage default TABLESPACE "USERS";
ALTER USER manage QUOTA 50M ON users;
GRANT manage_role TO manage;
```

（2）以hr登录到pdborcl,将12个表的增改查的权限设置给用户manage：
```
$ sqlplus hr/123@pdborcl
GRANT SELECT,UPDATE,INSERT ON good TO manage;
GRANT SELECT,UPDATE,INSERT ON type TO manage;
GRANT SELECT,UPDATE,INSERT ON user_table TO manage;
GRANT SELECT,UPDATE,INSERT ON car TO manage;
GRANT SELECT,UPDATE,INSERT ON orders TO manage;
GRANT SELECT,UPDATE,INSERT ON comments TO manage;
GRANT SELECT,UPDATE,INSERT ON address TO manage;
GRANT SELECT,UPDATE,INSERT ON supplier TO manage;
GRANT SELECT,UPDATE,INSERT ON transport TO manage;
GRANT SELECT,UPDATE,INSERT ON store TO manage;
GRANT SELECT,UPDATE,INSERT ON stock TO manage;
GRANT SELECT,UPDATE,INSERT ON stgood TO manage;
```


（3）登录manage创建视图：
```
$ sqlplus manage/123@pdborcl
CREATE VIEW store_view AS SELECT * FROM hr.good WHERE sid IN(SELECT sid FROM hr.stgood WHERE storeid=4);
CREATE VIEW stock_view AS SELECT * FROM hr.stock WHERE spid=2;
CREATE VIEW user_view AS SELECT * FROM hr.user_table WHERE userid=1;
```

2.角色：商家\
权限：可以查看自己店售卖的商品视图\
步骤：\
（1）创建一个用户store04；
```
$ sqlplus system/123@pdborcl
CREATE USER store04 IDENTIFIED BY 123;
```
（2）将视图store_view授权给store04:
```
GRANT SELECT,UPDATE,INSERT ON good TO store04;
GRANT SELECT,UPDATE,INSERT ON stgood TO store04;
GRANT SELECT ON store_view TO store04;
```
（3）登录store04查询视图:
```
SELECT * FROM store_view;
```

3.角色：供应商\
权限：可以查看自己的库存视图\
步骤：\
（1）创建一个用户supper02；
```
$ sqlplus system/123@pdborcl
CREATE USER supper02 IDENTIFIED BY 123;
```
（2）将视图stock_view授权给supper02:
```
GRANT SELECT ON stock_view TO supper02;
```
（3）登录supper02查询视图:
```
SELECT * FROM stock_view;
```

4.角色：普通用户\
权限：可以查看个人信息，可以增删查购物车、订单、地址，可以调用存储过程和函数\
步骤：\
（1）创建一个用户user01；
```
$ sqlplus system/123@pdborcl
CREATE USER user01 IDENTIFIED BY 123456;
```
（2）将视图user_view授权给user01:
```
GRANT SELECT ON user_view TO user01;
```
（3）登录user01 查询视图:
```
SELECT * FROM user_view;
```
（4）将hr的存储过程授权给user01:
```
GRANT ALL ON MyPack.Login TO user01;
```
（5）user01调用存储过程
```
set serveroutput on
DECLARE
MYNAME VARCHAR2(20);
MYPASSWORD VARCHAR2(20);
VALID VARCHAR2(20);   
BEGIN
MYNAME:='user1';
MYPASSWORD:='123456';
MyPack.Login (MYNAME=>myname,MYPASSWORD=>mypassword,VALID=>valid) ;    
DBMS_OUTPUT.PUT_LINE(VALID);
END;
```

## 三、PL/SQL设计
1.创建一个包(Package)，包名是MyPack。
```sql
create or replace PACKAGE MyPack IS
PROCEDURE Login(MYNAME IN VARCHAR2,MYPASSWORD IN VARCHAR2,VALID OUT VARCHAR2);
PROCEDURE Search_By_type(TYPENAME VARCHAR2);
PROCEDURE Modify_Addr(ORDERID IN NUMBER,ADDRID IN NUMBER,MESSGE OUT VARCHAR2);
FUNCTION Get_All_Price(MYID NUMBER) RETURN NUMBER;
END MyPack;
/
```


2.在MyPack中创建一个过程Login,输入参数是用户名和密码，进行登录验证
```sql
create or replace PACKAGE BODY MyPack IS
PROCEDURE Login(MYNAME IN VARCHAR2,MYPASSWORD IN VARCHAR2,VALID OUT VARCHAR2)
AS
    name1 VARCHAR2(20);
    pass VARCHAR2(20);
  BEGIN
    SELECT uname INTO name1 FROM user_table WHERE uname=MYNAME;
    SELECT upassword INTO pass FROM user_table WHERE uname=MYNAME AND upassword=MYPASSWORD;
    IF (name1!=MYNAME)
    THEN 
    VALID:='用户未注册';
    ELSIF(pass!=MYPASSWORD)
    THEN
    VALID:='密码错误';
    ELSE 
    VALID:='登录成功';
    END IF;
  END;
```  
```sql
-- 测试存储过程
set serveroutput on
DECLARE
MYNAME VARCHAR2(20);
MYPASSWORD VARCHAR2(20);
VALID VARCHAR2(20);   
BEGIN
MYNAME:='user1';
MYPASSWORD:='123456';
MyPack.Login (MYNAME=>myname,MYPASSWORD=>mypassword,VALID=>valid) ;    
DBMS_OUTPUT.PUT_LINE(VALID);
END;
/
```
结果分析：输入用户名和密码正确，返回验证信息“登录成功”。

3.在MyPack中创建一个过程Search_By_type，输入参数是分类名，通过分类查询商品，商品有二级分类，若查询的是一级父类，则输出该分类所有子类商品
```sql
PROCEDURE Search_By_type(TYPENAME VARCHAR2)
AS
    typeid NUMBER;
    fatherid NUMBER;
    cursor mycur1(father NUMBER) is SELECT sname,sinfo,sprice FROM good WHERE stid IN(SELECT tid FROM type WHERE ftid=father);
    cursor mycur2(childid NUMBER) is   SELECT sname,sinfo,sprice FROM good WHERE stid=childid;
  BEGIN
    SELECT tid INTO typeid FROM type WHERE tname=TYPENAME;
    SELECT ftid INTO fatherid FROM type WHERE tid=typeid;
    IF (fatherid=0) 
    THEN
        --使用游标
    for v in mycur1(fatherid)
    LOOP
        DBMS_OUTPUT.PUT_LINE(V.sname||
                            V.sinfo||' '||v.sprice);
    END LOOP;
    ELSE 
      --使用游标
    for v in mycur2(typeid)
    LOOP
        DBMS_OUTPUT.PUT_LINE(V.sname||
                            V.sinfo||' '||v.sprice);
    END LOOP;
    END IF;
  END;

-- 测试存储过程
set serveroutput on
DECLARE
    TYPENAME VARCHAR2(20);  
BEGIN
    TYPENAME:='饰品';
    MyPack.search_by_type(TYPENAME);
END;
```
结果分析：因为手链和项链都是属于饰品这个大类的，所有输出所有二级类包含商品。

4.在MyPack中创建一个过程Modify_Addr，更改订单地址，输入参数是订单号，以及修改后的地址ID，若是已发货则无法再修改地址，若修改成功则显示该订单的商品名称，付款总金额，收货人地址，收货人电话
```sql
PROCEDURE Modify_Addr(ORDERID IN NUMBER,ADDRID IN NUMBER,MESSGE OUT VARCHAR2)
AS
    hasid NUMBER;
    orderstatus VARCHAR(20);
  BEGIN
    SELECT oid INTO hasid FROM orders WHERE oid=ORDERID;
    SELECT ostatus INTO orderstatus FROM orders WHERE oid=ORDERID;
    IF(hasid!=ORDERID)
    THEN
    MESSGE:='未查询到订单信息';
    ELSIF (orderstatus!='待发货')
    THEN
    MESSGE:='已发货订单不可更改';
    ELSE
    UPDATE orders SET cid=ADDRID WHERE oid=ORDERID;
    MESSGE:='更改成功';
    END IF;
  END;

-- 测试存储过程
set serveroutput on
DECLARE
    ORDERID NUMBER;
    ADDRID NUMBER;
    MESSGE VARCHAR2(50);
  BEGIN
    ORDERID:=2;
    ADDRID:=3;
    MyPack.modify_addr(ORDERID=>orderid,ADDRID=>addrid,MESSGE=>messge);
    DBMS_OUTPUT.PUT_LINE(MESSGE);
  END;
  /
```
结果分析：因为编号为2的订单已发货，故不可再更改地址，返回信息“已发货订单不可更改”

5.在MyPack中创建一个函数Get_All_Price，输入参数是用户ID，计算该用户购物车物品总价
```sql
FUNCTION Get_All_Price(MYID NUMBER) RETURN NUMBER
AS
    allprice NUMBER DEFAULT 0;
  BEGIN
    SELECT SUM(saleprice*num) INTO allprice FROM car WHERE userid=MYID;
    RETURN allprice;
  END;
END MyPack;
/

-- 测试函数
set serveroutput on
DECLARE
    MYID NUMBER;
    ALLPRICE NUMBER;
  BEGIN
    MYID:=1;
    ALLPRICE:=MyPack.get_all_price(MYID=>MYID);
    DBMS_OUTPUT.PUT_LINE(ALLPRICE);
  END;
  /
```
结果分析：1号用户购物车商品总价为1176.28元。

## 四、备份方案
1.导出数据\
（1）先在服务器上创建一个目录
```
$ mkdir /home/oracle/oracle_bak
```
（2）以system登录到oracel
```
$ sqlplus system/123@oracel
```
（3）创建逻辑目录
```
SQL> create directory data_dir as ‘/home/oracle/oracle_bak’;
```
（4）查看管理员目录是否存在
```
SQL> select * from dba_direcories;
```
（5）使用管理员用户给指定的用户赋予在该目录的操作权限
```
SQL> grant read,write on directory data_dir to C##BAK_TEST_USER;
```
（6）全量导出数据库
```
$ expdp system/oracle@orcl dumpfile=expdp.dmp directory=data_dir full=y logfile=expdp.log
```
2.导入数据\
(1)首先将需要导入的数据文件存放导需要导入的数据库服务器上\
(2)参照导出的时候的建立目录方式建立物理目录和逻辑目录（只是建目录即可，如果需要给用户权限则加上给用户权限的那步）\
(3)使用命令进行全量导入
```
impdp user/passwd directory=data_dir dumpfile=expdp.dmp full=y
```

