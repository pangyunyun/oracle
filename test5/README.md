# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 脚本代码

```sql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); 
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![t5_1](t5_1.png)

## 测试

```sh

函数Get_SalaryAmount()测试：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                           4400
           20 Marketing                              19000
           30 Purchasing                             24900
           40 Human Resources                       6500
           50 Shipping                              156400
           60 IT                                    28800
           70 Public Relations                         10000
           80 Sales                                304500
           90 Executive                              58000
          100 Finance                               51608
          110 Accounting                             20308

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          120 Treasury                                   
          130 Corporate Tax                              
          140 Control And Credit                           
          150 Shareholder Services                         
          160 Benefits                                    
          170 Manufacturing                               
          180 Construction                                
          190 Contracting                                 
          200 Operations                                  
          210 IT Support                                 
          220 NOC                                       

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          230 IT Helpdesk                                
          240 Government Sales                           
          250 Retail Sales                                 
          260 Recruiting                                   
          270 Payroll                                     
```
![t5_2](t5_2.png)
```sh

过程Get_Employees()测试：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 102;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
102 Lex
    103 Alexander
        104 Bruce
        105 David
        106 Valli
        107 Diana

PL/SQL 过程已成功完成。
```
![t5_3](t5_3.png)

## 实验结论
1. 一般来讲，过程和函数的区别在于函数可以有一个返回值；而过程没有返回值。但过程和函数都可以通过out指定一个或多个输出参数。我们可以利用out参数，在过程和函数中实现返回多个值。
2. 如果只有一个返回值，用存储函数；否则，就用存储过程。
3. 一般不在存储过程和存储函数中做提交和回滚操作。
4. 如果一个存储过程中含有很多个返回值，需要在存储过程中全部写来，比较的不方便，所以可以让存储过程中返回一个指针进行处理，即包。
5. 如果在应用程序中经常需要执行特定的操作,可以基于这些操作简历一个特定的过程.通过使用过程可以简化客户端程序的开发和维护,而且还能提高客户端程序的运行性能。
6. 创建的存储过程一般包含三个模块，即声明部分、执行部分、异常处理部分,语法如下：
   ```sql
   -- OR REPLACE 表示，如果已经存在改存储过程，那么创建的存储过程hi直接替换原有的
   CREATE OR REPLACE procedure 存储过程名(参数1,参数2,...参数n) is/as   -- is as可以互换
       BEGIN
         SQL语句;
       EXCEPTION  
         SQL语句;
   end 存储过程名;
   ```
7. 创建函数与存储过程的形式上有些相似，也是编译后放在内存供用户使用，调用函数时要用表达式，而存储过程不用，函数必须要有一个返回值，存储过程没有。语法如下：
   ```sql
   create or replace function 函数名称(参数1 参数1类型,参数2 参数2类型) return 函数的返回值类型 is
   函数的内部变量(可选项);
   begin
     sql语句;
   exception
     异常处理代码;
   end;
   ```